const fs = require('fs');

const json = JSON.parse(fs.readFileSync('./data.json'));

console.log(json);

json.push({
    name: 'Yassine',
    age: 22,
    email: 'contact@youssefifkiren.me'
});

fs.writeFileSync('./data.json', JSON.stringify(json), (err) => {
    if (err) throw err;
    console.log('fichier enregistré');
});